Making a release essentially consists of converting GNOME's BuildStream projects to use tarballs instead of git, building it to make sure all the tarballs actually build together, publishing the result on master.gnome.org, and then announcing the release.

# Your First Release

You will need:

 * GNOME git account
 * Shell access to master.gnome.org with `ftpadmin` group (ability to modify `/ftp/pub/GNOME`)
 * Familiarity with release process for individual GNOME modules
 * Familiarity with building GNOME using BuildStream (TODO: migrate https://wiki.gnome.org/Newcomers/BuildSystemComponent)

Put the following in your ~/.ssh/config:

```
Host *.gnome.org
	User yourgnomeusername
```

Clone the gnome-build-meta and releng repos:

```
$ git clone git@gitlab.gnome.org:GNOME/gnome-build-meta.git
$ git clone git@gitlab.gnome.org:GNOME/releng.git
```

Install BuildStream. The easiest way is to use the toolbox container produced by freedesktop-sdk:

```
$ toolbox create -i registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2:latest
$ toolbox enter bst2-latest
```

# Simple release walkthrough

As an example, let's assume Wanda is making the 46.beta release.

## Before you start

As an aside, the canonical version identifiers for `GNOME N` are `N.alpha`, `N.beta`, `N.rc`, `N.0`, `N.1`... notice the 0 in `N.0`.


## Run tarball conversion

 * Create a new branch in gnome-build-meta from the appropriate branch (the master branch for alpha or beta releases, or the stable branch otherwise):
 
```
$ git fetch
$ git switch -w wanda/46.beta origin/master
```

 * cd into releng/tools/smoketesting
 * Run convert-to-tarballs.py to create the versions file and update the BuildStream project. This script examines every element in gnome-build-meta that uses git, and attempts to replace it with a tarball instead, respecting the tarball locations and version limits specified in tarball-conversion.config (or tarball-conversion-$VERSION.config):
 
 ```
 $ ./convert-to-tarballs.py -v 46.beta ~/path/to/gnome-build-meta/
 ```
 
  * If convert-to-tarballs.py fails with an error message, you may need to update tarball-conversion.config if there are new download locations, module renames, or new modules. Note that "Can't update tarball for module..." messages are not errors
  * For the x.0 stable release, you must create a tarball-conversion-$VERSION.config file (copy it from tarball-conversion.config). You may do so earlier (for beta or rc releases) if you need to set version limits.
  * Copy the versions file to the right file one directory up: versions, versions-stable or versions-oldstable
  * For stable branch releases, review the output of `git diff` and check for problematic tarball version updates. It is important to add version limits in tarball-conversion-$VERSION.confg to ensure development releases do not creep into your stable release! The limit attribute works by specifying the too-new version. E.g. if 47 is the current development stream, you want to put limit="47" to restrict your stable GNOME 46 release to picking up only the latest 46.y tarball. You may need to rerun tarball conversion several times until you are happy with the converted result.
  * Track elements in gnome-build-meta:

```
$ bst source track --deps=all iso/image.bst gnomeos/filesystem.bst gnomeos/filesystem-devel.bst
```

# Build the moduleset

 * You have two options for how to build the release. Pick whichever method you prefer:
   * Build locally first, and then create a merge request after you have a successful build. This is a good option if you have a fast computer.
   * Push your branch immediately and rely on the CI to build it. This is a good option if your computer is not very fast. Amend your commits and force push as needed to fix build failures:
   ```
   $ git commit --amend
   $ git push -f
   ```
 * Whether building locally or via CI, you'll probably encounter several build failures, which you'll need to fix, either by manually updating the tarball versions used in the BuildStream projects, or hounding maintainers to make new releases and then re-running ./convert-to-tarballs.py, or adding patches (which is the least-desirable option). Dealing with build failures is the most frustrating part of the release process. Don't hesitate to manually downgrade tarball versions as needed to get a successful build, but if you do so, be sure to nag maintainers to get the problem fixed before the next release.
 * If you choose to build the release locally:
   * Start by closing your open workspaces, which will pollute your build if you forget to close them:
   
   ```
   bst workspace close --all
   ```

   * Start the build:
   
   ```
   $ bst --strict build core.bst sdk.bst
   ```
 * For alpha releases, do not create a merge request; skip to the next step. For all other releases, once you have a successful build, create a merge request. For beta releases, the merge request should initially target the master branch. For all other releases, target the appropriate stable branch.
 * The CI will build the release for you. Continue to the next step once your CI is green.
 
# Update releng

 * Update the version-controlled versions file under releng/tools (one directory above smoketesting), then push your changes to the releng repository. For unstable releases:
 
```
$ cp tools/smoketesting/versions tools/versions
$ git commit -a -m 'GNOME 46.beta'
```

For stable releases, copy your versions file to tools/versions-stable instead. For oldstable releases, copy it to tools/versions-oldstable.

# Updating gnome-build-meta

 * If doing a beta release, make sure that all merge requests relevant to both the master and new stable branch have been merged if possible. Then create the new stable branch (e.g. gnome-46). You can do this from the GitLab UI by going to Repository > Branches in the GitLab sidebar. Change the target branch of your merge request to the new branch.
 * Try to get another member of the release team to review your merge request and merge it.
 * For alpha releases, do not create a tag; skip to the next step. For all other releases, create an annotated tag (`git tag -a`) and push it. You can also create the tag from the GitLab UI by going to Repository > Tags in the sidebar and writing something in the message field.

# Publishing on master.gnome.org

 * Create a git archive with the contents of gnome-build-meta:

```
$ git fetch
$ git archive --prefix gnome-46.beta/ -o gnome-46.beta.tar 46.beta
$ xz gnome-46.beta.tar
```

 * Copy files to master.gnome.org, then ssh in:

```
$ scp gnome-build-meta/gnome-46.beta.tar.xz master.gnome.org:
$ scp releng/tools/smoketesting/versions master.gnome.org:
$ ssh master.gnome.org
```

 * Install the release. When running `simple-news`, the first argument is the previous release, and the second argument is the current release:

```
$ mkdir /ftp/pub/GNOME/teams/releng/46.beta
$ cp ~/gnome-46.beta.tar.xz ~/versions /ftp/pub/GNOME/teams/releng/46.beta
$ ftpadmin release-suites 46.beta ~/versions
$ ftpadmin simple-news 46.alpha 46.beta
```

 * If doing a beta, rc, or .0 release, mirror the VM image that is produced by the s3-image job. Wait for the CI pipeline for the tag to finish, then:

```
$ cd ~/
$ curl -O -L https://os.gnome.org/download/46.beta/gnome_os_installer_46.beta.iso
$ mkdir /ftp/pub/GNOME/gnomeos/46.beta
$ cp gnome_os_installer_46.beta.iso /ftp/pub/GNOME/gnomeos/46.beta
```

Make sure to download the file into your homedir and then use `cp` instead of `mv`, as `mv` can cause issues with the file permissions.

 * Announce the release in [the Desktop category on Discourse](https://discourse.gnome.org/tags/c/desktop/6/announcement), using the "announcement" tag. If you mirrored the VM image, include a link to the VM image ([example](https://download.gnome.org/gnomeos/41.0/gnome_os_installer_46.beta.iso)) in the announcement.

# Extra steps for x.beta release

 * Check on the various other release-related efforts: release notes, video, website, press package, etc. Check in with the engagement team to make sure these are on track.

# Extra steps for x.0 release

 * Remember to mention the release name (most recent GUADEC/GNOME Asia host city)
 * Make sure www.gnome.org is updated to note the new release
 * Make sure the release notes are finalized and published on release.gnome.org/X (best to coordinate in the Engagement matrix room)
 * Spot-check if www.gnome.org/getting-gnome looks up-to-date wrt. to distro releases and GNOME versions
 * Check if there's a release video, and if so link it in your announcement (ask Caroline)
 * Announce on Discourse (coordinate with the Engagement room for social media, Caroline has been handling this)
 * Make sure the new schedule is available as ics (git location: gnomeweb-wml/www.gnome.org/start/schedule-unstable.ics) and on the wiki. The ical.py creates this, see releng module, tools/schedule directory. new www.gnome.org still redirects to old GNOME for the ics file ([instructions for creating schedule](https://gitlab.gnome.org/GNOME/releng/-/blob/master/tools/schedule/README?ref_type=heads))
 * Update `DEFAULT_SCHEDULE` in tools/schedule/libschedule.py in the releng module to reference the next release cycle, to keep automatic release reminders working (Michael again)

# Extra steps for x.1 release

Mark the previous release runtime as end of life as of the date that x.0 was released ([example](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/commit/8fff01ce7dc7f2ae66ee0aaec361206895c4657f)). Confusingly, the schedule displays the final release alongside x.0 as the EOL date, which we'll honor in the EOL message, but we don't actually mark the runtime as EOL until x.1.
