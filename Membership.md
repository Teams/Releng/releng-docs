# Release Team Membership

(Alphabetical Order)

 * Abderrahim Kitouni - gnome-build-meta maintenance, doing releases (affiliation: CodeThink)
 * Andre Klapper - tracking incoming bugs + marking blocker bugs + sending mail about them; trying to keep an eye on user docs and L10N (affiliation: Wikimedia Foundation)
 * Federico Mena Quintero - GNOME Foundation Board of Directors liaison
 * Javier Jardon - doing releases, GNOME CI infrastructure, gnome-build-meta metadatada maintenance, gnome-build-meta CI, project wide tasks, review freeze break requests (affiliation: CodeThink)
 * Jeremy Bicha - doing releases, downstream packaging (affiliation: Canonical)
 * Jordan Petridis - doing releases, gnome-build-meta maintenance, CI infrastructure, Nightlies Repo (affiliation: Centricular)
 * Matthias Clasen - doing releases; scanning incoming bugs; tracking blocker bugs and sending mail about them. (affiliation: Red Hat)
 * Sophie Herold - managing app categorization reviews and QA (affiliation: none)
 
When members leave the release team, they should be added to the emeritus section below.

The release team is not directly elected, but should be representative of the GNOME community. Membership is normally by invite and recommendation when one person leaves. In theory, the GNOME Foundation board has the power to select its members and influence its decisions, but they usually don't fix stuff if it isn't broken. Not more than 40% of release team members can directly or indirectly have the same employer affiliation (similar to section 2.d of the GNOME foundation bylaws).

A member usually leaves the release team when they feel not useful anymore or when they have not been active for two development cycles. If you are leaving, look for somebody to replace you. Avoiding stagnation (i.e., release team with always the same members) would also be great, so if you feel you can help the release team by joining, you can ask around if it's a good idea and it might just happen.

The GNOME Foundation Board of Directors appoints one member of the release team to serve as a liaison to the Board. For purposes of the 40% affiliation rule, the corporate affiliation of the liaison shall be considered to be "GNOME Foundation" regardless of the liaison's actual employer. The Board liaison is a special role on release team that is expected to change frequentely and where performing release team work is not expected, so the liaison is generally not added to the release team emeritus list when they leave release team.

# Release Team Emeritus

Here is a list of people who were in the release team in the past (excluding Board liasons).

(Please keep this list in reverse chronological order of the date they left the team for new departures. We're not certain about the order of older release team members.)

 * Michael Catanzaro
 * Tristan Van Berkom
 * Emmanuele Bassi
 * Olav Vitters
 * Kjartan Maraas
 * Colin Walters
 * Frederic Peters
 * Luca Ferretti
 * Alejandro Piñeiro
 * Vincent Untz
 * Lucas Rocha
 * Frédéric Crozat
 * Karsten Bräckelmann
 * John Palmieri
 * Elijah Newren
 * Jeff Waugh
 * Federico Mena-Quintero
 * Luis Villa
 * Jonathan Blandford
 * Murray Cumming
 * Mark McLoughlin
 * Andrew Sobala
 * Glynn Foster
 * Mikael Hallendal
 * Calum Benson
 * John Fleck
 * Sander Vesik
 * Seth Nickell
 * Michael Meeks
 * Gregory Leblanc
 * Jody Goldberg
 * Telsa Gwynne
 * Maciej Stachowiak
 * Leslie Proctor
 * Ian T. Peters
 * Dan Mueth
 * Jamin P. Gray
 * Karl Gaffney
 * Greg Corrin
 * Jacob Berkman
